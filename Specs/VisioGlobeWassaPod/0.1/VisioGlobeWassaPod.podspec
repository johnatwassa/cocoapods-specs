#
# Be sure to run `pod lib lint VisioGlobeWassaPod.podspec' to ensure this is a
# valid spec and remove all comments before submitting the spec.
#
# Any lines starting with a # are optional, but encouraged
#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = "VisioGlobeWassaPod"
  s.version          = "0.1.1"
  s.summary          = "Wassa Internal Pod for implementing VisioGlobe into an iOS project"

  s.homepage         = "https://github.com/<GITHUB_USERNAME>/VisioGlobeWassaPod"
  s.license          = 'MIT'
  s.author           = { "Maxime Bernard" => "maximebernardo@gmail.com" }
  s.source           = { :git => "https://MaxBernard@bitbucket.org/johnatwassa/sdk-indoor-location-ios.git", :tag => 'v0.1' }


  s.platform            = :ios, '7.0'
  s.requires_arc        = true

  s.source_files        = 'Pod/Classes/**/*.{h,m,mm,cpp}'
  s.resources    = ['Pod/Assets/**/*']


  s.prefix_header_file  = 'Pod/Classes/pch/VisioNavigationModule_Prefix.pch'

  s.header_mappings_dir = 'Pod/Classes/include'
 
#s.public_header_files = 'Pod/Classes/include/VgEngine/*.h'
#'Pod/Classes/External/Jsoncpp/include/json/' ,
#'Pod/Classes/External/zlib/include/' ,
#                          'Pod/Classes/samples/Common/Inc/',
#                          'Pod/Classes/samples/navigationModule/Inc/Apple/'

  s.frameworks          = 'CoreGraphics' , 'Foundation' , 'QuartzCore' , 'OpenGLES' , 'UIKit' , 'Security', 'CoreLocation', 'CoreBluetooth', 'CoreMotion'

  s.vendored_libraries  = 'Pod/Classes/vgLib/libVisioMove.a',
                          'Pod/Classes/vgLib/libminizip.a',
                          'Pod/Classes/vgLib/libJsoncpp.a',
			  'Pod/Classes/Polestar/libNaoLocationService.a'

  #s.framework = libz
  s.libraries = 'z'


  s.dependency           'SFHFKeychainUtils'
  s.dependency           'UIView+NibLoading'
  s.dependency           'AFNetworking'
  s.dependency           'HMSegmentedControl'
  s.dependency           'GSKeychain'
  s.dependency           'Reachability'
  s.dependency           'JGProgressHUD'

  s.xcconfig            = {
                              'CLANG_CXX_LANGUAGE_STANDARD'  => 'c++98',
                              'CLANG__STANDARD'              => 'c++98',
                              'CLANG_CXX_LIBRARY'            => 'libstdc++',
                              'GCC_C_LANGUAGE_STANDARD'      => 'c99',
                              'CLANG_ENABLE_MODULES'         => 'NO',
                              'COMPRESS_PNG_FILES'           => 'NO',
                              'DEAD_CODE_STRIPPING'          => 'NO',
                              'GCC_ENABLE_CPP_RTTI'          => 'NO',
                              'GCC_PRECOMPILE_PREFIX_HEADER' => 'YES',
                              'OTHER_LDFLAGS'                => '-ObjC',
                              'OTHER_LIBTOOLFLAGS'           => '-ObjC'
                           }

end


